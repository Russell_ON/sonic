// JavaScript Document
const superBtn = document.querySelector('.btn-gems');
const ear = document.querySelector('.svg');
const rechterWenkbrauw = document.querySelector('.svg-rechts');
const container = document.querySelector('.container');
const haar = document.querySelector('.haar');
const sonic = document.querySelector('.sonic');
const ring = document.querySelector('.btn-ring');
const amount = document.querySelector('.amount');
const audio = new Audio("sound/Super Sonic - Sonic the Hedgehog 2 [OST].mp3");
const collectRing = new Audio("sound/ringcollect.mp3");


var ringCount = 0;
var isSuper = false;
var rings = [];

//Voeg 10 ringen toe aan de html en voeg ze vervolgens aan een array toe
for(i = 0; i < 10; i++){
    var ringsElement = container.appendChild(ring.cloneNode());
    rings.push(ringsElement);

    ring.remove();
}


//functie om een random waarde te krijgen
function getRandom(min,max){
    return Math.floor(Math.random() * (max-min + 1) + min );
}

//functie om de ringen een random positie te geven 
function getRandomLocation(ring){
    ring.style.left= getRandom(0, container.clientWidth - 100)+'px';
    ring.style.top= getRandom(0, container.clientHeight - 100)+'px';
}


//loop over de array en zorg ervoor dat alle ringen een click event hebben
function createRings(){
    rings.forEach(function(ring){
        ring.addEventListener("click", function(e){
            ringCount++;
            collectRing.load();
            collectRing.volume = 0.05;
            collectRing.play();
            amount.textContent = ringCount;
            getRandomLocation(e.target);
        })
        
        getRandomLocation(ring);
    })
}


//functie om te checken of sonic in ze supermode zit
function checkSuper(){
    if(isSuper == true){
        ringCount--;
        amount.textContent = ringCount;

        console.log("nu is hij super");


        //als de ringcount 0 is moet hij weer terug gaan naar normaal
        if(ringCount === 0){
            isSuper = false;
            animation();
            removeSuper();

            console.log("hij is nu geen super")
        }
    }

    //als hij geen super is moet de button niet meer werken
    if(isSuper == false){

        console.log("nu moet hij geen super zijn")
        if(ringCount === 0){
            superBtn.disabled = true;
        }else{
            superBtn.disabled = false;
        }
    }
}

//om de seconden wordt deze funtie hier uitgevoerd
setInterval(function(){ 
    checkSuper();

}, 1000);


//function om de dom te manipuleren
function addSuper(){
    isSuper = true;
    document.documentElement.style.setProperty('--main-sonic-color', '#FFF601');
    ear.classList.add("ear-super");
    rechterWenkbrauw.classList.add("rechterwenkbrauw-super");
    haar.classList.add('super');
    sonic.classList.add('glow');
    audio.play();
    audio.volume = 0.012;
    toggle = false;
}

//functie om de dom te manipuleren
function removeSuper(){
    isSuper = false;
    document.documentElement.style.setProperty('--main-sonic-color', '#0072bb');
    ear.classList.add("ear");
    rechterWenkbrauw.classList.add("rechterwenkbrauw");
    haar.classList.remove('super');
    sonic.classList.remove('glow');
    rechterWenkbrauw.classList.remove("rechterwenkbrauw-super");
    audio.pause();
    audio.currentTime = 0;
    toggle = true;

}


function animation(){
    container.classList.add('flash');

    container.addEventListener('animationend', function(){
        container.classList.remove('flash')

    })
}


let toggle = true

//als je op de gems klinkt wordt hij super:)!
superBtn.addEventListener('click', function(){
    animation();

    if(toggle){
        addSuper()
    }else{
        removeSuper();
    }
})

window.addEventListener("DOMContentLoaded", function(){
    createRings();
})